﻿using Common.Interfaces;
using System;
using System.Collections.Generic;

namespace Common.Implementations
{
    public class ArrayHelper : IArrayHelper
    {
        public (int index, int value) GetMaxElement(int[] arr, int start, int end)
        {
            if (arr.Length < 2) throw new ArgumentException(nameof(arr));

            int middle = (start + end + 1) / 2;
            if (middle == end) // it means that I need to compare 2 elements arr[start] and arr[middle]
            {
                return arr[start] > arr[middle] ? (start, arr[start]) : (middle, arr[middle]);
            }
            if (arr[middle] > arr[start]) // 
            {
                return GetMaxElement(arr, middle, end); // it means that maximum element  is contained in the right side
            }
            else
            {
                return GetMaxElement(arr, start, middle - 1); // it means that maximum element  is contained in the left side
            }
        }

        public (int startIndex, int endIndex) GetMaxInternalArray(int[] arr)
        {
            if (arr == null || arr.Length == 0) throw new ArgumentException(nameof(arr));

            Dictionary<int, int> sumToIndexes = new Dictionary<int, int>();
            int sum = 0;
            (int startIndex, int endIndex) result = (0, 0);

            for (int i = 0; i < arr.Length; i++)
            {
                sum += arr[i];
                if (sumToIndexes.ContainsKey(sum))
                {
                    if (i - sumToIndexes[sum] > result.endIndex - result.startIndex)
                    {
                        result = (sumToIndexes[sum] + 1, i);
                    }
                }
                else if (!sumToIndexes.ContainsKey(sum))
                {
                    sumToIndexes.Add(sum, i);
                }
            }
            return result;

        }

        public void MergeSort(int[] arr)
        {
            MergeSorting(arr, 0, arr.Length - 1);
        }

        // Find a triplet such that sum of two equals to third element
        public int GetTripletsCount(int[] arr)
        {
            int res = 0;

            // Simple approach: Run three loops and check if there exists a triplet such that sum of two elements equals the third element.
            // Time complexity: O(n ^ 3)

            // Array.Sort(arr);

            //for (int i = arr.Length - 1; i >= 0; i--)
            //{
            //    for (int j = i - 1; j >= 0; j--)
            //    {
            //        for (int k = j -1; k >= 0; k--)
            //        {
            //            if (arr[i] == (arr[j] + arr[k]))
            //            {
            //                res++;
            //            }
            //        }
            //    }

            //}

            // TODO: Oleg
            // Does it time complexity: O(n ^ 2)? https://www.bigocheatsheet.com/
            Array.Sort(arr); // O(n ^ 2)?

            for (int i = arr.Length - 1; i >= 0; i--)
            {
                int j = i - 1;
                int k = 0;

                while (k < j)
                {
                    if (arr[i] == arr[j] + arr[k])
                    {
                        res++;
                        j--;
                        k++;
                    }
                    else if (arr[j] + arr[k] > arr[i])
                    {
                        j--;
                    }
                    else
                    {
                        k++;
                    }
                }
            }


            if (res == 0) return -1;
            return res;
        }

        // Rotate a square matrix clockwise in place (without creating a new array)
        // 1  2  3  4     00 01 02 03
        // 5  6  7  8     10 11 12 13
        // 9 10 11 12     20 21 22 23
        // 13 14 15 16    30 31 32 33

        // 13  9  5  1    30 20 10 00
        // 14 10  6  2    31 21 11 01
        // 15 11  7  3    32 22 12 02
        // 16 12  8  4    33 23 13 03

        public void RotateSquareMatrix(int[][] arr)
        {
            for (int i = 0; i < arr.Length / 2; i++)
            {
                Swap(arr, i);
            }
        }

        private void Swap(int[][] arr, int counter)
        {
            for (int i = counter; i < arr.Length - 1 - counter; i++)
            {
                int tmp = arr[counter][i]; // left top
                arr[counter][i] = arr[arr.Length - 1 - i][counter]; //left bottom
                arr[arr.Length - 1 - i][counter] = arr[arr.Length - 1 - counter][arr.Length - 1 - i]; // right bottom
                arr[arr.Length - 1 - counter][arr.Length - 1 - i] = arr[i][arr.Length - 1 - counter]; // right top
                arr[i][arr.Length - 1 - counter] = tmp; // left top
            }
        }

        // Given an array arr of N integers. Find the contiguous sub-array with maximum sum.
        public int GetMaxSubArraySum(int[] a)
        {
            int size = a.Length;
            int max = int.MinValue,
                currentMax = 0;

            for (int i = 0; i < size; i++)
            {
                currentMax += a[i];

                if (max < currentMax)
                    max = currentMax;

                if (currentMax < 0)
                    currentMax = 0;
            }

            return max;

            //if (arr == null || arr.Length < 1) throw new ArgumentException(nameof(arr));

            //int max = arr[0];
            //int currentSum = arr[0];
            //int step = arr[0];

            //if (arr.Length == 1) return max;

            //for (int i = 1; i < arr.Length; i++)
            //{
            //    if (arr[i] > )
            //    if (currentSum + arr[i] >= step)
            //    {
            //        currentSum += arr[i];
            //    } else
            //    {
            //        if (i + i < arr.Length)
            //            i++;
            //        currentSum = arr[i];
            //        step = arr[i];
            //    }

            //    if (max < currentSum)
            //    {
            //        max = currentSum;
            //    }

            //}
            //return max;
        }

        private void MergeSorting(int[] arr, int left, int right)
        {
            if (left < right)
            {
                int middle = left + (right - left) / 2;
                MergeSorting(arr, left, middle);
                MergeSorting(arr, middle + 1, right);
                Merge(arr, left, middle, right);
            }
        }

        private void Merge(int[] arr, int left, int middle, int right)
        {
            int i, j, k;
            int leftSize = middle - left + 1;
            int rightSize = right - middle;
            int[] leftArray = new int[leftSize];
            int[] rightArray = new int[rightSize];

            k = left;
            for (i = 0; i < leftSize; i++)
            {
                leftArray[i] = arr[k];
                k++;
            }

            k = middle + 1;
            for (j = 0; j < rightSize; j++)
            {
                rightArray[j] = arr[k];
                k++;
            }

            i = 0; j = 0;
            k = left;

            while (i < leftSize && j < rightSize)
            {
                if (leftArray[i] < rightArray[j])
                {
                    arr[k] = leftArray[i];
                    i++;
                }
                else
                {
                    arr[k] = rightArray[j];
                    j++;
                }
                k++;
            }

            while (i < leftSize)
            {
                arr[k] = leftArray[i];
                i++;
                k++;
            }

            while (j < rightSize)
            {
                arr[k] = rightArray[j];
                j++;
                k++;
            }

        }

        private void Swap(int[] arr, int left, int right)
        {
            if (arr.Length < left && arr.Length < right && left != right)
            {
                var tmp = arr[left];
                arr[left] = arr[right];
                arr[right] = tmp;
            }
        }
    }
}
