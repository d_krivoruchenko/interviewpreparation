﻿using Common.Implementations;
using Common.Interfaces;
using Common.Models;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace Tests
{
    public class NodeTests
    {
        private INodeHelper _nodeHelper;

        private static Node node0 = new Node(0);
        private static Node node1 = new Node(1);
        private static Node node2 = new Node(2);
        private static Node node3 = new Node(3);
        private static Node node4 = new Node(4);
        private static Node node5 = new Node(5);
        private static Node node6 = new Node(6);
        private static Node node7 = new Node(7);
        private static Node node8 = new Node(8);
        private static Node node9 = new Node(9);

        public NodeTests()
        {
            _nodeHelper = new NodeHelper();
            node0.Next = node1;
            node1.Next = node2;
            node2.Next = node3;
            node3.Next = node4;
            node4.Next = node0;

            node5.Next = node6;
            node6.Next = node7;
            node7.Next = node8;
            node8.Next = node9;
            node9.Next = null;
        }

        [Theory]
        [MemberData(nameof(NodeHasCircleCorrectTestData))]
        public void GetMaxElement_Correct(Node node, bool expectedResult)
        {
            var actualResult = _nodeHelper.HasCircle(node);
            Assert.Equal(expectedResult, actualResult);
        }

        [Theory]
        [MemberData(nameof(RemoveNElemetFromEndCorrectTestData))]
        public void RemoveNElemetFromEnd_Correct(Node node, int position, int expectedResult)
        {
            var actualResult = _nodeHelper.RemoveNElemetFromEnd(node, position);

            Assert.False(ContainValue(actualResult, expectedResult));
        }

        public static IEnumerable<object[]> NodeHasCircleCorrectTestData =>
            new List<object[]>
            {
                new object[] { node0, true },
                new object[] { node5, false },
            };

        private static bool ContainValue(Node node, int value)
        {
            if (node == null) return false;
            while (node.Next != null)
            {
                if(node.Value == value)
                {
                    return true;
                }
                node = node.Next;
            }
            return false;
        }

        public static IEnumerable<object[]> RemoveNElemetFromEndCorrectTestData =>
            new List<object[]>
            {
                new object[] { node5, 2, 8 },
                new object[] { node5, 1, 9 },
                new object[] { node5, 5, 5 },
            };
    }
}
