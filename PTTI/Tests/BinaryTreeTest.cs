﻿using Common.Implementations;
using Common.Interfaces;
using Common.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using Xunit;

namespace Tests
{
    public class BinaryTreeTest
    {
        private IBinaryTreeHelper<int> _binaryTreeHelper;
        private static BinaryTree<int> _tree = new BinaryTree<int>();
        public BinaryTreeTest()
        {
            _binaryTreeHelper = new BinaryTreeHelper<int>();
            _tree.Add(12);
            _tree.Add(8);
            _tree.Add(15);
            _tree.Add(4);
            _tree.Add(10);
            _tree.Add(13);
            _tree.Add(19);
            _tree.Add(1);
            _tree.Add(6);
            _tree.Add(14);
            _tree.Add(18);
            _tree.Add(20);
            _tree.Add(5);
        }

        [Theory]
        [MemberData(nameof(GetBinaryTreeDepthCorrectTestData))]
        public void GetBinaryTreeDepth_Correct(BinaryTree<int> tree, int expectedResult)
        {
            var actualResult = _binaryTreeHelper.GetBinaryTreeDepth(tree);
            Assert.Equal(expectedResult, actualResult);
        }

        [Theory]
        [MemberData(nameof(GetBinaryTreeDepthCorrectTestData))]
        public void ConvertBinarySearchTreeToSortedDoubleLinkedList_Correct(BinaryTree<int> tree, int expectedResult)
        {

            _binaryTreeHelper.ConvertBinarySearchTreeToSortedDoubleLinkedList(tree);
            // See output
            PrintRightBinaryTree(tree.Head);
            // TODO: modify assert
            Assert.NotNull(expectedResult);
            
        }

        public static IEnumerable<object[]> GetBinaryTreeDepthCorrectTestData =>
            new List<object[]>
            {
                new object[] { _tree, 5},
            };

        private void PrintRightBinaryTree(BinaryTreeNote<int> node)
        {
            StringBuilder stringBuilder = new StringBuilder();

            var head = node;

            // left part

            while (node != null)
            {
                stringBuilder.Insert(0, $"{node.Value} --> ");
                node = node.Left;
            }

            node = head;
            // right part 
            while (node != null)
            {
                if(node.Right != null)
                {
                    stringBuilder.Append($"{node.Value} --> ");
                } else
                {
                    stringBuilder.Append($"{node.Value}");
                }
                
                node = node.Right;
            }
            Debugger.Log(1, "Info", stringBuilder.ToString());
        }
    }
}
