﻿using Common.Implementations;
using Common.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace Tests
{
    public class StringTest
    {
        private IStringHelper _stringHelper;

        public StringTest()
        {
            _stringHelper = new StringHelper();
        }

        [Theory]
        [MemberData(nameof(GetPermutationsCorrectTestData))]
        public void GetPermutations_Correct(string arr, List<string> expectedResult)
        {
            var actualResult = _stringHelper.GetPermutations(arr);
            actualResult = actualResult.OrderBy(x => x).ToList(); ;

            expectedResult = expectedResult.ToList().OrderBy(x => x).ToList();
            Assert.Equal(expectedResult, actualResult);
        }

        public static IEnumerable<object[]> GetPermutationsCorrectTestData =>
            new List<object[]>
            {
                new object[] { "ABC", new List<string> { "ABC", "ACB", "BAC", "BCA", "CAB", "CBA" } },
            };
    }
}
