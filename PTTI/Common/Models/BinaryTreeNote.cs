﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Models
{
    public class BinaryTreeNote<T> : IComparable<T> where T : IComparable<T>
    {
        public BinaryTreeNote(T value)
        {
            Value = value;
        }

        public T Value { get; private set; }

        public BinaryTreeNote<T> Left { get; set; }

        public BinaryTreeNote<T> Right { get; set; }

        public int CompareTo(T other)
        {
            return Value.CompareTo(other);
        }
    }
}
