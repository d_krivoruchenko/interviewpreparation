﻿using Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Interfaces
{
    public interface IBinaryTreeHelper<T> where T: IComparable<T>
    {
        int GetBinaryTreeDepth(BinaryTree<T> tree);
        void ConvertBinarySearchTreeToSortedDoubleLinkedList(BinaryTree<T> tree);
    }
}
