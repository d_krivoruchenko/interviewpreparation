﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Common.Models
{
    public class BinaryTree<T> : IEnumerable<T> where T : IComparable<T>
    {
        private int _count;

        public int Count() => _count;

        public BinaryTreeNote<T> Head { get; private set; }

        public void Add(T value)
        {
            if (Head == null)
            {
                Head = new BinaryTreeNote<T>(value);
            }
            else
            {
                AddTo(Head, value);
            }
            _count++;
        }

        private void AddTo(BinaryTreeNote<T> note, T value)
        {
            if (value.CompareTo(note.Value) < 0)
            {
                if (note.Left == null)
                {
                    note.Left = new BinaryTreeNote<T>(value);
                }
                else
                {
                    AddTo(note.Left, value);
                }
            }
            else
            {
                if(note.Right == null)
                {
                    note.Right = new BinaryTreeNote<T>(value);
                } else
                {
                    AddTo(note.Right, value);
                }
            }
        }

        public IEnumerator<T> GetEnumerator()
        {
            return null;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return null;
        }
    }
}
