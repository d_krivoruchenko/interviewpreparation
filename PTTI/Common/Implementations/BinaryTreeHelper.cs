﻿using Common.Interfaces;
using Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Implementations
{
    public class BinaryTreeHelper<T> : IBinaryTreeHelper<T> where T : IComparable<T>
    {
        public BinaryTreeNote<T> root;
        public static BinaryTreeNote<T> prev = null;
        public BinaryTreeNote<T> head;
        /*
         *  maxDepth()
         *  1. If tree is empty then return 0
         *  2. Else
         *  (a) Get the max depth of left subtree recursively  i.e., 
         *  call maxDepth( tree->left-subtree)
         *  (a) Get the max depth of right subtree recursively  i.e., 
         *  call maxDepth( tree->right-subtree)
         *  (c) Get the max of max depths of left and right 
         *  subtrees and add 1 to it for the current node.
         *  max_depth = max(max dept of left subtree, max depth of right subtree) + 1
         *  (d) Return max_depth
         */

        public int GetBinaryTreeDepth(BinaryTree<T> tree)
        {
            return GetBinaryTreeDepthRecursive(tree.Head);
        }

        public void ConvertBinarySearchTreeToSortedDoubleLinkedList(BinaryTree<T> tree)
        {
            ConvertBinarySearchTreeToSortedDoubleLinkedList(tree.Head);
        }

        private void ConvertBinarySearchTreeToSortedDoubleLinkedList(BinaryTreeNote<T> root)
        {
            if (root == null)
            {
                return;
            }

            ConvertBinarySearchTreeToSortedDoubleLinkedList(root.Left);

            if(prev == null)
            {
                head = root;
            } else
            {
                root.Left = prev;
                prev.Right = root;
            }
            prev = root;
            ConvertBinarySearchTreeToSortedDoubleLinkedList(root.Right);
        }

        private int GetBinaryTreeDepthRecursive(BinaryTreeNote<T> note)
        {
            if (note == null) return 0;

            int leftDepth = GetBinaryTreeDepthRecursive(note.Left);
            int rightDepth = GetBinaryTreeDepthRecursive(note.Right);

            return Math.Max(leftDepth, rightDepth) + 1;
        }
    }
}
