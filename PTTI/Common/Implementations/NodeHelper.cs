﻿using Common.Interfaces;
using Common.Models;
using System;

namespace Common.Implementations
{
    public class NodeHelper : INodeHelper
    {
        public bool HasCircle(Node head)
        {
            if (head == null) throw new ArgumentException(nameof(head));
            Node temp = head.Next;
            while (temp.Next != null && head != temp)
            {
                temp = temp.Next;
            }
            
            return head == temp;
        }

        // Please write a function that will remove the Nth element from the end in the linked list.
        // Do we have an end in this linked list?
        // Can we have a circle in this linked list?
        // Do I need to return a head?
        // Will a deletion position always be available? Can this position be more than node length?
        // Complexity O(n)

        public Node RemoveNElemetFromEnd(Node node, int position)
        {
            if (node == null) throw new ArgumentNullException("Node can not be null", nameof(node));

            int nodeLength = 1;
            Node head = node;

            while (node.Next != null)
            {
                node = node.Next;
                nodeLength++;
            }

            if (position > nodeLength) throw new ArgumentException("Position to remove can not be more than node length", nameof(position));

            if(position == nodeLength)
            {
                return head.Next;
            }

            node = head;
            int currentPosition = 1;
            while(node?.Next != null)
            {
                if(currentPosition == (nodeLength - position))
                {
                    if (node.Next != null)
                    {
                        node.Next = node.Next.Next;
                    } else
                    {
                        node.Next = null;
                    }
                    
                }
                node = node.Next;
                currentPosition++;
            }

            return head;
        }
    }
}
