﻿using Common.Models;

namespace Common.Interfaces
{
    public interface INodeHelper
    {
        bool HasCircle(Node node);
        Node RemoveNElemetFromEnd(Node node, int position);
    }
}
