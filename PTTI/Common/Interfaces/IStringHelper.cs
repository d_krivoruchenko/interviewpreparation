﻿using System.Collections.Generic;

namespace Common.Interfaces
{
    public interface IStringHelper
    {
        IEnumerable<string> GetPermutations(string arg);
    }
}
