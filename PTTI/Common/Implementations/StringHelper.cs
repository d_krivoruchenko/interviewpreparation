﻿using Common.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Common.Implementations
{
    public class StringHelper : IStringHelper
    {
        public IEnumerable<string> GetPermutations(string arg)
        {
            if (arg.Length == 0) return Enumerable.Empty<string>();

            var result = new List<string>();

            if (arg.Length == 1)
            {
                result.Add(arg);
            }
            else if (arg.Length > 1)
            {
                int lastIndex = arg.Length - 1;
                // Find out the last character
                String last = arg.Substring(lastIndex);
                // Rest of the string
                String rest = arg.Substring(0, lastIndex);
                // Perform permutation on the rest string and
                // merge with the last character
                result = Merge(GetPermutations(rest), last);
            }
            return result;
        }

        public static List<string> Merge(IEnumerable<string> list, string c)
        {
            List<string> res = new List<string>();
            // Loop through all the string in the list
            foreach(String s in list)
            {
                // For each string, insert the last character to all possible positions


                // and add them to the new list
                for (int i = 0; i <= s.Length; ++i)
                {
                    String ps = s.Insert(i, c);
                    res.Add(ps);
                }
            }
            return res;
        }

        private void Permutate(string arg, int left, int right, List<string> result)
        {
            if (left == right)
            {
                result.Add(arg);
            }
            else
            {
                Permutate(arg, left + 1, right, result);
                for (int i = left + 1; i < right; i++)
                {
                    Swap(arg, left, i);
                    Permutate(arg, left + 1, right, result);
                    Swap(arg, left, i);
                }
            }
        }

        private string Swap(string str, int left, int right)
        {
            var arr = str.ToArray();
            if (arr.Length > left && arr.Length > right && left != right)
            {
                char tmp = arr[left];
                arr[left] = arr[right];
                arr[right] = tmp;
            }
            return arr.ToString();
        }
    }
}
