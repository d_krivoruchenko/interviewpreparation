using Common.Implementations;
using Common.Interfaces;
using System;
using System.Collections.Generic;
using Xunit;

namespace Tests
{
    public class ArrayTest
    {
        private IArrayHelper _arrayHelper;

        public ArrayTest()
        {
            _arrayHelper = new ArrayHelper();
        }

        [Theory]
        [MemberData(nameof(ArrayGetMaxSubArraySumCorrectTestData))]
        public void GetMaxSubArraySumt_Correct(int[] arr, int expectedResult)
        {
            var actualResult = _arrayHelper.GetMaxSubArraySum(arr);
            Assert.Equal(expectedResult, actualResult);
        }

        [Theory]
        [MemberData(nameof(ArrayRotateSquareMatrixCorrectTestData))]
        public void RotateSquareMatrix_Correct(int[][] arr, int[][] expectedResult)
        {
            _arrayHelper.RotateSquareMatrix(arr);
            _arrayHelper.RotateSquareMatrix(arr);
            _arrayHelper.RotateSquareMatrix(arr);
            _arrayHelper.RotateSquareMatrix(arr);
            Assert.Equal(expectedResult, arr);
        }
        

        [Theory]
        [MemberData(nameof(ArrayGetTripletsCountCorrectTestData))]
        public void GetTripletsCount_Correct(int[] arr, int expectedResult)
        {
            var actualResult = _arrayHelper.GetTripletsCount(arr);
            Assert.Equal(expectedResult, actualResult);
        }

        [Theory]
        [MemberData(nameof(ArrayGetMaxElementCorrectTestData))]
        public void GetMaxElement_Correct(int[] arr, (int index, int value) expectedResult)
        {
            var actualResult = _arrayHelper.GetMaxElement(arr, 0, arr.Length);
            Assert.Equal(expectedResult, actualResult);
        }

        [Theory]
        [MemberData(nameof(ArrayGetMaxElementIncorrectTestData))]
        public void GetMaxElement_Incorrect(int[] arr)
        {
            var exception = Assert.Throws<ArgumentException>(() => _arrayHelper.GetMaxElement(arr, 0, arr?.Length ?? 0));
            Assert.True(exception is ArgumentException);
        }

        [Theory]
        [MemberData(nameof(ArrayGetMaxInternalArrayCorrectTestData))]
        public void GetMaxInternalArray_Correct(int[] arr, (int startIndex, int endIndex) expectedResult)
        {
            var actualResult = _arrayHelper.GetMaxInternalArray(arr);
            Assert.Equal(expectedResult, actualResult);
        }

        [Theory]
        [MemberData(nameof(ArrayMergeSortingCorrectTestData))]
        public void MergeSorting_Correct(int[] arr, int[] result)
        {
            _arrayHelper.MergeSort(arr);
            Assert.Equal(result, arr);
        }

        public static IEnumerable<object[]> ArrayGetMaxElementCorrectTestData =>
            new List<object[]>
            {
                    new object[] { new[] { 4, 5, 6, 1, 2, 3 }, (index: 2, value: 6) },
                    new object[] { new[] { 5, 6, 7, 8, 3 }, (index: 3, value: 8) },
                    new object[] { new[] { 4, 5, 6, 6, 2, 3 }, (index: 3, value: 6) },
                    new object[] { new[] { 0, 0, 0, 0, 0, 0 }, (index: 0, value: 0) },
                    new object[] { new[] { -1, -2, -3, -4, -5, -6 }, (index: 0, value: -1) },
                    new object[] { new[] { 4, 5, 6, 6, 2, 3 }, (index: 3, value: 6) },
                    new object[] { new[] { 4, 5, 6, 7, 8, 3 }, (index: 4, value: 8) },
                    new object[] { new[] { 4, 5, 6, 7, 8, 3 }, (index: 4, value: 8) },
                    new object[] { new[] { 4, 3 }, (index: 0, value: 4) },
                    new object[] { new[] { 14, 5, 6}, (index: 0, value: 14) },
                    new object[] { new[] { 14, 13 }, (index: 0, value: 14) },
                    new object[] { new[] { 1, 2, 3, 4, 5, 6, 0 }, (index: 5, value: 6) },
            };
        public static IEnumerable<object[]> ArrayGetMaxElementIncorrectTestData =>
            new List<object[]>
            {
                    new object[] { new[] { 4 } },
                    new object[] { new int [] { } },
            };
        public static IEnumerable<object[]> ArrayMergeSortingCorrectTestData =>
            new List<object[]>
            {
                new object[] { new[] { 4,2,1,3,5,8,7,6 }, new[] { 1, 2, 3, 4, 5, 6, 7, 8 } },
                new object[] { new[] { 11, 1, 1, 4,2,1,3,5,8,7,6 }, new[] { 1, 1, 1, 2, 3, 4, 5, 6, 7, 8, 11 } },
            };

        public static IEnumerable<object[]> ArrayGetMaxInternalArrayCorrectTestData =>
            new List<object[]>
            {
                new object[] { new[] { 4, 2, 1, -1, -2, 3 }, (startIndex: 1, endIndex: 4) },
                new object[] { new[] { 4, 2, 1, -1, -2, 3, 1, 2, 3, 4, 5, 6, -6, -4, -3, -5 , -3, -1, 0 , 0, 0, 1, 2 }, (startIndex: 3, endIndex: 21) },
                // new object[] { new[] { 0, 0, 0, 0, 0 }, (startIndex: 0, endIndex: 4) }, fix it
            };

        public static IEnumerable<object[]> ArrayGetTripletsCountCorrectTestData =>
            new List<object[]>
            {
                new object[] { new[] { 5, 32, 1, 7, 10, 50, 19, 21, 2 }, 2 },
                new object[] { new[] { 5, 32, 1, 7, 10, 50, 19, 21, 0 }, -1 },
                new object[] { new[] { 1, 5, 3, 2 }, 2 },
            };

        public static IEnumerable<object[]> ArrayGetMaxSubArraySumCorrectTestData =>
            new List<object[]>
            {
                new object[] { new[] { 1, 2, 3, -2, 5 }, 9 },
                new object[] { new[] { -5, -32, -1, -7, -10, -50, -19, -21, 19 }, 19 },
                new object[] { new[] { -1, -2, -3, -4 }, -1 },
                new object[] { new[] { -1, -2, -3, -4, 0 }, 0 },
                new object[] { new[] { 2, 0, 3, -3, 5 }, 7 },
                new object[] { new[] { -1, -2, -3, 4, -3, 2, -1, 3 }, 5},
                new object[] { new[] { -1, 2, 0, 4, -3, 2, -1, 3 }, 7},
            };
        public static IEnumerable<object[]> ArrayRotateSquareMatrixCorrectTestData =>
            new List<object[]>
            {
                new object[] { GenerateSquareMatrix(5), GenerateSquareMatrix(5) },
                new object[] { GenerateSquareMatrix(4), GenerateSquareMatrix(4) },
                new object[] { GenerateSquareMatrix(3), GenerateSquareMatrix(3) },
                new object[] { GenerateSquareMatrix(2), GenerateSquareMatrix(2) },
                new object[] { GenerateSquareMatrix(1), GenerateSquareMatrix(1) },
            };

        private static int[][] GenerateSquareMatrix(int size)
        {
            int[][] arr = new int[size][];
            for (int i = 0; i < size; i++)
            {
                arr[i] = new int[size];
                for (int j = 0; j < size; j++)
                {
                    arr[i][j] = (i * size) + 1 + j;
                }
            }
            return arr;
        }
    }
}
