﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Interfaces
{
    public interface IArrayHelper
    {
        (int index, int value) GetMaxElement(int[] arr, int start, int end);
        (int startIndex, int endIndex) GetMaxInternalArray(int[] arr);
        void MergeSort(int[] array);
        int GetTripletsCount(int[] arr);
        int GetMaxSubArraySum(int[] arr);
        void RotateSquareMatrix(int[][] arr);
    }
}
